from ubuntu:18.04

LABEL maintainer Alexander Merck <alexander.t.merck@gmail.com>
LABEL name "glastopf"
LABEL version "0.1"
LABEL release "1"
LABEL summary "Glastopf Honeypot container"
LABEL description "Glastopf is a Python web application honeypot"
LABEL authoritative-source-url "https://github.com/CommunityHoneyNetwork/glastopf"
LABEL changelog-url "https://github.com/CommunityHoneyNetwork/glastopf/commits/master"

# Set DOCKER var - used by Glastopf init to determine logging
ENV DOCKER "yes"
ENV playbook "glastopf.yml"

RUN apt-get update \
    && apt-get install -y ansible python-apt

RUN echo "localhost ansible_connection=local" >> /etc/ansible/hosts
ADD . /opt/
RUN ansible-playbook /opt/${playbook}

# Do this for containers only
RUN sed -i 's/^DAEMONIZE=""/DAEMONIZE="-n"/' /opt/glastopf/bin/glastopf-runner

ENTRYPOINT ["/usr/bin/runsvdir", "-P", "/etc/service"]